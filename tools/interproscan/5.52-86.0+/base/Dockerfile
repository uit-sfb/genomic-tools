#The reason why we base our image on pftools instead of ubuntu:20.04
#is because in interpro 5.52, pftools is broken. Since it is difficult to build pftools, we simply use it as our base image
#But this is just a hack, so please using ubuntu:20.04 again when pftools have ben updated
FROM registry.gitlab.com/uit-sfb/genomic-tools/pftools:0.0.0

ARG version

LABEL maintainer="mmp@uit.no" \
  toolVersion=$version \
  toolName=InterProScan \
  url=https://github.com/ebi-pf-team/interproscan

USER root

#For tzdata setting
ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Oslo
RUN echo $TZ > /etc/timezone && \
    apt-get update && apt-get install -y tzdata && \
    rm /etc/localtime && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata && \
    apt-get clean

#libpcre-dev is needed by pfsearchV3 which uses libpcre.so
RUN apt-get update -y && apt-get install perl wget bash python3 libtemplate-plugin-digest-md5-perl software-properties-common libpcre3-dev libgomp1 -y \
&& add-apt-repository ppa:openjdk-r/ppa \
&& apt install -y openjdk-11-jdk

WORKDIR /app/interpro

RUN wget -qO- ftp://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/${version}/alt/interproscan-core-${version}.tar.gz | tar xz --strip-components 1 \
&& rm -rf data \
&& chown -R :root /app/interpro \
&& chmod -R g=rwx /app/interpro

#Remove me when pftools have been updated!!
RUN ln -sf /var/lib/pftools/bin/pfscan bin/prosite/pfscan && \
    ln -sf /var/lib/pftools/bin/pfscanV3 bin/prosite/pfscanV3 && \
    ln -sf /var/lib/pftools/bin/pfsearch bin/prosite/pfsearch && \
    ln -sf /var/lib/pftools/bin/pfsearchV3 bin/prosite/pfsearchV3 && \
    ln -sf /var/lib/pftools/bin/ps_scal.pl bin/prosite/ps_scal.pl

ENTRYPOINT ["./interproscan.sh"]

#### IMPORTANT
#To test interpro (recommanded!), install interpro dbs in <DB_PATH>
#and run docker run --rm -v <DB_PATH>:/app/interpro/data registry.gitlab.com/uit-sfb/genomic-tools/interproscan:<VERSION> -i test_all_appl.fasta -f tsv -dp

